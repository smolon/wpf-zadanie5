﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_zadanie5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Uzytkownik> uzytkownicy = new List<Uzytkownik>();
        public Uzytkownik uzytkownikDialog = new Uzytkownik();
        public string ImieMW;
        public string NazwiskoMW;
        public string EmailMW;
        public static bool IsPodgladOpen = false;
        public Dialog2 dlg2 = new Dialog2();

        private void DodajDoList(string _Imie, string _Nazwisko, string _Email)
        {
            Uzytkownik usr = new Uzytkownik(_Imie, _Nazwisko, _Email);
            uzytkownicy.Add(usr);
            uzytkownicy_listbox.Items.Refresh();
           
        }
        
        public MainWindow()
        {
            dlg2.Visibility = Visibility.Hidden;
            InitializeComponent();
            uzytkownicy.Add(new Uzytkownik("adam", "solon", "23@bla.com"));
            uzytkownicy.Add(new Uzytkownik("asd", "fsd", "bvbv@bla.com"));
            uzytkownicy.Add(new Uzytkownik("zxc", "sowerlon", "kyu@bla.com"));
            usun_btn.IsEnabled = false;
            edytuj_btn.IsEnabled = false;
            podglad_btn.IsEnabled = false;
            uzytkownicy_listbox.ItemsSource = uzytkownicy;           
        }

        private void Dodaj_btn_Click(object sender, RoutedEventArgs e)
        {
            Dialog1 dlg = new Dialog1(false, "", "", "")
            {
                Owner = this
            };
            dlg.ShowDialog();
            DodajDoList(ImieMW, NazwiskoMW, EmailMW);
        }

        private void Usun_btn_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz usunąć dany element z listy? \nTa operacja jest nieodwracalna!", "Usuń element", 
                MessageBoxButton.YesNo, MessageBoxImage.Exclamation, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.No:
                    break;
                case MessageBoxResult.Yes:
                    uzytkownicy.Remove(uzytkownicy_listbox.SelectedItem as Uzytkownik);
                    uzytkownicy_listbox.Items.Refresh();
                    MessageBox.Show("Pomyślnie sunięto element", "Sukces!",MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
        }

        private void Edytuj_btn_Click(object sender, RoutedEventArgs e)
        {
            Uzytkownik usr = uzytkownicy_listbox.SelectedItem as Uzytkownik;
            Dialog1 dlg = new Dialog1(true, usr.Imie, usr.Nazwisko, usr.Email)
            {
                Owner = this
            };
            dlg.ShowDialog();
            
            if (!dlg.IsCanceled)
            {
                usr.Imie = ImieMW;
                usr.Nazwisko = NazwiskoMW;
                usr.Email = EmailMW; 
            }
            uzytkownicy_listbox.Items.Refresh();
        }

        private void Zamknij_btn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Uzytkownicy_listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (uzytkownicy_listbox.SelectedItems == null)
            {
                edytuj_btn.IsEnabled = false;
                usun_btn.IsEnabled = false;
                podglad_btn.IsEnabled = false;
            }
            else
            {
                edytuj_btn.IsEnabled = true;
                usun_btn.IsEnabled = true;
                podglad_btn.IsEnabled = true;
            }
            if (IsPodgladOpen)
            {
                dlg2.imie_textbox_podglad.Text = uzytkownicy[uzytkownicy_listbox.SelectedIndex].Imie;
                dlg2.nazwisko_textbox_podglad.Text = uzytkownicy[uzytkownicy_listbox.SelectedIndex].Nazwisko;
                dlg2.email_textbox_podglad.Text = uzytkownicy[uzytkownicy_listbox.SelectedIndex].Email;
                dlg2.Index = uzytkownicy_listbox.SelectedIndex;
            }
        }

        public void ProcessImie(string data)
        {
            uzytkownicy[uzytkownicy_listbox.SelectedIndex].Imie = data;
            uzytkownicy_listbox.Items.Refresh();
        }

        public void ProcessNazwisko(string data)
        {
            uzytkownicy[uzytkownicy_listbox.SelectedIndex].Nazwisko = data;
            uzytkownicy_listbox.Items.Refresh();
        }

        public void ProcessEmail(string data)
        {
            uzytkownicy[uzytkownicy_listbox.SelectedIndex].Email = data;
            uzytkownicy_listbox.Items.Refresh();
        }

        private void Podglad_btn_Click(object sender, RoutedEventArgs e)
        {
            dlg2.Owner = this;
            IsPodgladOpen = true;
            dlg2.Visibility = Visibility.Visible;
            dlg2.Show();
        }
    }
}
