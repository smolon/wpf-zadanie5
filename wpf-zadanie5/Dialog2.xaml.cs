﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpf_zadanie5
{
    /// <summary>
    /// Interaction logic for dialog2.xaml
    /// </summary>
    public partial class Dialog2 : Window
    {
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public int Index { get; set; }
        public Dialog2()
        {
            InitializeComponent();
            this.Visibility = Visibility.Visible;
        }
        private void close_btn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.IsPodgladOpen = false;
            this.Visibility = Visibility.Hidden;
        }

        private void Imie_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            mainWindow.ProcessImie(imie_textbox_podglad.Text);
        }
        
        private void Nazwisko_textbox_podglad_TextChanged(object sender, TextChangedEventArgs e)
        {
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            mainWindow.ProcessNazwisko(nazwisko_textbox_podglad.Text);
        }

        private void Email_textbox_podglad_TextChanged(object sender, TextChangedEventArgs e)
        {
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            mainWindow.ProcessEmail(email_textbox_podglad.Text);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            imie_textbox_podglad.Text = Imie;
            nazwisko_textbox_podglad.Text = Nazwisko;
            email_textbox_podglad.Text = Nazwisko;
        }

        private void Dialog2_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            MainWindow.IsPodgladOpen = false;
            this.Visibility = Visibility.Hidden;
        }
    }
}
