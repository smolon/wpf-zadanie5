﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpf_zadanie5
{
    /// <summary>
    /// Interaction logic for dialog1.xaml
    /// </summary>
    public partial class Dialog1 : Window
    {
        bool IsEditiing;
        public bool IsCanceled;
        public Dialog1(bool type, string Imie, string Nazwisko, string Email)
        {
            InitializeComponent();
            IsEditiing = type;
            this.Title = "Dodaj";
            if (IsEditiing)
            {
                this.Title = "Edytuj";
                imie_textbox.Text = Imie;
                nazwisko_textbox.Text = Nazwisko;
                email_textbox.Text = Email;
            }
        }
        string _Imie = "";
        string _Nazwisko = "";
        string _Email = "";
        public string Imie
        {
            get { return _Imie; }
        }
        public string Nazwisko
        {
            get { return _Nazwisko; }
        }
        public string Email
        {
            get { return _Email; }
        }

        private void Anuluj_btn_Click(object sender, RoutedEventArgs e)
        {
            IsCanceled = true;
            Close();
        }

        private void Ok_btn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            mainWindow.ImieMW = imie_textbox.Text;
            mainWindow.NazwiskoMW = nazwisko_textbox.Text;
            mainWindow.EmailMW = email_textbox.Text;
            Close();
        }
    }
}
