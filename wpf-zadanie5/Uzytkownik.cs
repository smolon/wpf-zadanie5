﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_zadanie5
{
    public class Uzytkownik
    {
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }

        public Uzytkownik() { }
        public Uzytkownik(string _imie, string _nazwisko, string _email)
        {
            Imie = _imie;
            Nazwisko = _nazwisko;
            Email = _email;
        }
        public override string ToString()
        {
            return $"{Imie} {Nazwisko} {Email}";
        }
    }
}
